import * as universal from '../entries/pages/_page.ts.js';

export const index = 2;
let component_cache;
export const component = async () => component_cache ??= (await import('../entries/pages/_page.svelte.js')).default;
export { universal };
export const universal_id = "src/routes/+page.ts";
export const imports = ["_app/immutable/nodes/2.DMn9UcSM.js","_app/immutable/chunks/disclose-version.MNDP4g5i.js","_app/immutable/chunks/runtime.BNNiWLuX.js","_app/immutable/chunks/attributes.DK3KXeFd.js","_app/immutable/chunks/render.BtLtoY8T.js","_app/immutable/chunks/store.Zz2yjUvO.js","_app/immutable/chunks/index.BBbDMmNP.js"];
export const stylesheets = ["_app/immutable/assets/2.DxdA27wo.css"];
export const fonts = [];
