import * as server from '../entries/pages/sverdle/_page.server.ts.js';

export const index = 4;
let component_cache;
export const component = async () => component_cache ??= (await import('../entries/pages/sverdle/_page.svelte.js')).default;
export { server };
export const server_id = "src/routes/sverdle/+page.server.ts";
export const imports = ["_app/immutable/nodes/4.BxAGOe_R.js","_app/immutable/chunks/disclose-version.MNDP4g5i.js","_app/immutable/chunks/runtime.BNNiWLuX.js","_app/immutable/chunks/render.BtLtoY8T.js","_app/immutable/chunks/props.BCQujgp0.js","_app/immutable/chunks/attributes.DK3KXeFd.js","_app/immutable/chunks/store.Zz2yjUvO.js","_app/immutable/chunks/entry.CVr9QBpz.js","_app/immutable/chunks/index.BBbDMmNP.js"];
export const stylesheets = ["_app/immutable/assets/4.CBD7-3nv.css"];
export const fonts = [];
