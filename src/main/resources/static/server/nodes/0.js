

export const index = 0;
let component_cache;
export const component = async () => component_cache ??= (await import('../entries/pages/_layout.svelte.js')).default;
export const imports = ["_app/immutable/nodes/0.DEpvOe9T.js","_app/immutable/chunks/disclose-version.MNDP4g5i.js","_app/immutable/chunks/runtime.BNNiWLuX.js","_app/immutable/chunks/render.BtLtoY8T.js","_app/immutable/chunks/attributes.DK3KXeFd.js","_app/immutable/chunks/store.Zz2yjUvO.js","_app/immutable/chunks/stores.CZ_FGftS.js","_app/immutable/chunks/entry.CVr9QBpz.js","_app/immutable/chunks/index.BBbDMmNP.js"];
export const stylesheets = ["_app/immutable/assets/0.BZAaoqrJ.css"];
export const fonts = ["_app/immutable/assets/fira-mono-cyrillic-ext-400-normal.B04YIrm4.woff2","_app/immutable/assets/fira-mono-all-400-normal.B2mvLtSD.woff","_app/immutable/assets/fira-mono-cyrillic-400-normal.36-45Uyg.woff2","_app/immutable/assets/fira-mono-greek-ext-400-normal.CsqI23CO.woff2","_app/immutable/assets/fira-mono-greek-400-normal.C3zng6O6.woff2","_app/immutable/assets/fira-mono-latin-ext-400-normal.D6XfiR-_.woff2","_app/immutable/assets/fira-mono-latin-400-normal.DKjLVgQi.woff2"];
