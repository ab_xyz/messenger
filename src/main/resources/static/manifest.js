export const manifest = (() => {
function __memo(fn) {
	let value;
	return () => value ??= (value = fn());
}

return {
	appDir: "_app",
	appPath: "_app",
	assets: new Set(["favicon.png","robots.txt"]),
	mimeTypes: {".png":"image/png",".txt":"text/plain"},
	_: {
		client: {"start":"_app/immutable/entry/start.CRq6T6jI.js","app":"_app/immutable/entry/app.B2Du9JGi.js","imports":["_app/immutable/entry/start.CRq6T6jI.js","_app/immutable/chunks/entry.CVr9QBpz.js","_app/immutable/chunks/runtime.BNNiWLuX.js","_app/immutable/chunks/index.BBbDMmNP.js","_app/immutable/entry/app.B2Du9JGi.js","_app/immutable/chunks/runtime.BNNiWLuX.js","_app/immutable/chunks/render.BtLtoY8T.js","_app/immutable/chunks/disclose-version.MNDP4g5i.js","_app/immutable/chunks/props.BCQujgp0.js"],"stylesheets":[],"fonts":[],"uses_env_dynamic_public":false},
		nodes: [
			__memo(() => import('./server/nodes/0.js')),
			__memo(() => import('./server/nodes/1.js')),
			__memo(() => import('./server/nodes/4.js'))
		],
		routes: [
			{
				id: "/sverdle",
				pattern: /^\/sverdle\/?$/,
				params: [],
				page: { layouts: [0,], errors: [1,], leaf: 2 },
				endpoint: null
			}
		],
		matchers: async () => {
			
			return {  };
		},
		server_assets: {}
	}
}
})();

export const prerendered = new Set(["/","/about","/sverdle/how-to-play"]);
