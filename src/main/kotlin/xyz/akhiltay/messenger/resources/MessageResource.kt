package xyz.akhiltay.messenger.resources

import jakarta.inject.Inject
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import org.springframework.security.core.context.SecurityContextHolder
import xyz.akhiltay.messenger.*

@Path("/api/chats/{chatId}/messages")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
class MessageResource @Inject constructor(private val messageService: MessageService) {

    @GET
    fun getMessages(@PathParam("chatId") chatId: Long, @QueryParam("offset") @DefaultValue("0") offset: Int): Response {
        val messages = messageService.getMessages(chatId, offset = offset)
        return Response.ok(messages.map { MessageDto.fromEntity(it) }).build()
    }

    @POST
    fun createMessage(@PathParam("chatId") chatId: Long, messageDto: MessageShortDto): Response {
        if (messageDto.content.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build()
        }

        val user = SecurityContextHolder.getContext().authentication.principal as User
        val message = messageService.createMessage(chatId, user.id!!, messageDto.content, attachments = messageDto.attachments)
        return Response.status(Response.Status.CREATED).entity(MessageDto.fromEntity(message)).build()
    }

}