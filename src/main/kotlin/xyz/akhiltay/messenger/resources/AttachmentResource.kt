package xyz.akhiltay.messenger.resources

import jakarta.inject.Inject
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import org.apache.tika.Tika
import org.glassfish.jersey.media.multipart.FormDataContentDisposition
import org.glassfish.jersey.media.multipart.FormDataParam
import xyz.akhiltay.messenger.AttachmentService
import java.io.InputStream
import java.util.*

@Path("/api/attachments")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.MULTIPART_FORM_DATA)
class AttachmentResource @Inject constructor(private val attachmentService: AttachmentService) {
    private val tika = Tika()

    @POST
    fun uploadAttachment(
        @FormDataParam("file") inputStream: InputStream,
        @FormDataParam("file") fileDetail: FormDataContentDisposition
    ): Response {
        val maxFileSize = 20 * 1024 * 1024
        val fileBytes = inputStream.readBytes()

        if (fileBytes.size > maxFileSize) {
            return Response.status(Response.Status.BAD_REQUEST).entity("File size exceeds the limit of 20 MB").build()
        }

        val mimeType = tika.detect(fileBytes)
        if (!mimeType.startsWith("image/")) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Only image files are allowed").build()
        }

        val attachment = attachmentService.createAttachment(
            filename = fileDetail.fileName,
            contentType = mimeType,
            data = fileBytes
        )

        return Response.status(Response.Status.CREATED).entity(mapOf("id" to attachment.id)).build()
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    fun getAttachment(@PathParam("id") id: String): Response {
        val attachment = attachmentService.getAttachmentById(UUID.fromString(id))
        return if (attachment != null) {
            Response.ok(attachment.data).type(attachment.contentType).build()
        } else {
            Response.status(Response.Status.NOT_FOUND).build()
        }
    }
}