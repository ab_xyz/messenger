package xyz.akhiltay.messenger.resources


import jakarta.inject.Inject
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import org.springframework.security.core.context.SecurityContextHolder
import xyz.akhiltay.messenger.*

@Path("/api/chats")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
class ChatResource @Inject constructor(
    private val chatService: ChatService,
    private val chatMemberService: ChatMemberService
) {

    @GET
    @Path("/{id}")
    fun getChat(@PathParam("id") id: Long): Response {
        val chat = chatService.getById(id)
        return if (chat != null) {
            Response.ok(chat).build()
        } else {
            Response.status(Response.Status.NOT_FOUND).build()
        }
    }

    @POST
    fun createChat(chatDto: CreateChatDto): Response {
        val user = SecurityContextHolder.getContext().authentication.principal as User
        val chat = chatService.createChat(name = chatDto.name, description = chatDto.description)
        chatMemberService.addChatMember(chat.id!!, user.id!!, MemberRole.OWNER)
        return Response.status(Response.Status.CREATED).entity(ChatDto.fromEntity(chat)).build()
    }

    @DELETE
    @Path("/{id}")
    fun deleteChat(@PathParam("id") id: Long): Response {
        val user = SecurityContextHolder.getContext().authentication.principal as User
        return if (chatService.deleteChat(id, user.id!!)) {
            Response.noContent().build()
        } else {
            Response.status(Response.Status.FORBIDDEN).build()
        }
    }

    @GET
    @Path("/me")
    fun getUserChats(): Response {
        val user = SecurityContextHolder.getContext().authentication.principal as User
        val chats = chatService.getUserChats(user.id!!)
        return Response.ok(chats.map { ChatDto.fromEntity(it) }).build()
    }

    @POST
    @Path("/{id}/join")
    fun joinChat(@PathParam("id") chatId: Long): Response {
        val user = SecurityContextHolder.getContext().authentication.principal as User
        val chatMember = chatMemberService.addChatMember(chatId, user.id!!, MemberRole.MEMBER)
        return Response.ok(ChatMemberDto.fromEntity(chatMember)).build()
    }

    @GET
    @Path("/{id}/members")
    fun getChatMembers(@PathParam("id") id: Long): Response {
        val chatMembers = chatMemberService.getChatMembers(id)
        return Response.ok(chatMembers.map { UserDto.fromEntity(it.user) }).build()
    }
}
