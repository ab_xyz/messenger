package xyz.akhiltay.messenger.resources

import jakarta.inject.Inject
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import xyz.akhiltay.messenger.User
import xyz.akhiltay.messenger.UserDto
import xyz.akhiltay.messenger.UserService

@Path("/api/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
class UserResource @Inject constructor(private val userService: UserService) {
    @GET
    @Path("/{id}")
    fun getUser(@PathParam("id") id: Long): UserDto? = userService.getById(id)?.let { UserDto.fromEntity(it) }
}