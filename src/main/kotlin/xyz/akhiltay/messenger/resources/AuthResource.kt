package xyz.akhiltay.messenger.resources

import com.fasterxml.jackson.databind.ObjectMapper
import jakarta.inject.Inject
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.RequestBody
import xyz.akhiltay.messenger.*


@Path("/api/auth")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
class AuthResource @Inject constructor(private val authService: AuthService) {

    private val mapper: ObjectMapper = ObjectMapper()

    @POST
    @Path("/test")
    fun test(): Response {
        val res = mapper.createObjectNode()
        res.put("status", "success")
        return Response.ok(res).build()
    }

    @POST
    @Path("/register")
    fun register(user: UpdateUserDto): Response {
        val token: Token = authService.register(user)
        return Response.status(Response.Status.CREATED).entity(TokenDto.fromEntity(token)).build()
    }

    @POST
    @Path("/login")
    fun login(@RequestBody user: LoginUserDto): Response {
        val token: Token = authService.login(user)
        return Response.ok(TokenDto.fromEntity(token)).build()
    }

    @POST
    @Path("/logout")
    fun logout(@RequestBody tokenDto: TokenShortDto): Response {
        authService.logout(tokenDto.token)
        return Response.noContent().build()
    }

    @GET
    @Path("/me")
    fun me(): Response {
        val user = SecurityContextHolder.getContext().authentication.principal as User
        return Response.ok(user).build()
    }
}
