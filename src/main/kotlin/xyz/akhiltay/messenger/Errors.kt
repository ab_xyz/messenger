package xyz.akhiltay.messenger

open class ApiException(val key: String, message: String) : Exception(message)

class InvalidTokenException(message: String) : ApiException("invalid_token", message)

class WrongCredentialsException(message: String) : ApiException("wrong_credentials", message)