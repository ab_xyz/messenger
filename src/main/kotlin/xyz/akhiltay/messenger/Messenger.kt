package xyz.akhiltay.messenger

import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Messenger

fun main(args: Array<String>) {
	runApplication<Messenger>(*args) {
		setBannerMode(Banner.Mode.OFF)
	}
}