package xyz.akhiltay.messenger.configs

import org.springframework.context.annotation.Configuration
import org.springframework.http.server.ServerHttpRequest
import org.springframework.http.server.ServerHttpResponse
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.simp.SimpMessageType
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.messaging.simp.stomp.StompHeaderAccessor
import org.springframework.messaging.support.ChannelInterceptor
import org.springframework.messaging.support.MessageHeaderAccessor
import org.springframework.stereotype.Controller
import org.springframework.web.socket.WebSocketHandler
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
import org.springframework.web.socket.config.annotation.StompEndpointRegistry
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer
import org.springframework.web.socket.server.HandshakeInterceptor
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor
import xyz.akhiltay.messenger.AuthService
import xyz.akhiltay.messenger.ClientRegistryService
import xyz.akhiltay.messenger.Message
import xyz.akhiltay.messenger.User
import java.security.Principal

@Configuration
@EnableWebSocketMessageBroker
@Controller
class WebSocketConfig(
    private val authService: AuthService,
    private val clientRegistryService: ClientRegistryService
) : WebSocketMessageBrokerConfigurer {
//    override fun configureMessageBroker(registry: MessageBrokerRegistry) {
//        registry.enableSimpleBroker("/topic")
//        registry.setApplicationDestinationPrefixes("/app")
//    }

    override fun registerStompEndpoints(registry: StompEndpointRegistry) {
        registry.addEndpoint("/ws")
//            .addInterceptors(HttpSessionHandshakeInterceptor(), TokenHandshakeInterceptor(authService, clientRegistryService))
            .setAllowedOrigins("*")
            .withSockJS()
    }

//    override fun configureClientInboundChannel(registration: org.springframework.messaging.simp.config.ChannelRegistration) {
//        registration.interceptors(TokenChannelInterceptor(authService, clientRegistryService))
//    }
}

class TokenHandshakeInterceptor(
    private val authService: AuthService,
    private val clientRegistryService: ClientRegistryService
) : HandshakeInterceptor {

    override fun beforeHandshake(
        request: ServerHttpRequest,
        response: ServerHttpResponse,
        wsHandler: WebSocketHandler,
        attributes: MutableMap<String, Any>
    ): Boolean {
        val queryParams = request.uri.query
        val params = queryParams.split("&")
            .map { it.split("=") }
            .associate { it[0] to it[1] }

        val token = params["token"]
        if (token != null) {
            val user = authService.validateToken(token)
            attributes["user"] = user
            clientRegistryService.addClient(user.id!!, token)
        }
        return true
    }

    override fun afterHandshake(
        request: ServerHttpRequest,
        response: ServerHttpResponse,
        wsHandler: WebSocketHandler,
        exception: Exception?
    ) {
    }
}

class TokenChannelInterceptor(
    private val authService: AuthService,
    private val clientRegistryService: ClientRegistryService
) : ChannelInterceptor {

    override fun preSend(
        message: org.springframework.messaging.Message<*>,
        channel: MessageChannel
    ): org.springframework.messaging.Message<*>? {
        val accessor: StompHeaderAccessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor::class.java)!!
        if (SimpMessageType.CONNECT == accessor.messageType) {
            val token = accessor.getFirstNativeHeader("token")
            if (token != null) {
                val user = authService.validateToken(token)
                accessor.user = user as Principal
                clientRegistryService.addClient(user.id!!, token)
            }
        }
        return message
    }

    override fun afterSendCompletion(
        message: org.springframework.messaging.Message<*>,
        channel: MessageChannel,
        sent: Boolean,
        ex: Exception?
    ) {
        val accessor: StompHeaderAccessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor::class.java)!!
        if (SimpMessageType.DISCONNECT == accessor.messageType) {
            val user = accessor.user as User?
            if (user != null) {
                clientRegistryService.removeClient(user.id!!)
            }
        }
    }
}