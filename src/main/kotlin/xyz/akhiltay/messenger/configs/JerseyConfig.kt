package xyz.akhiltay.messenger.configs

import org.glassfish.jersey.media.multipart.MultiPartFeature
import org.glassfish.jersey.server.ResourceConfig
import org.springframework.context.annotation.Configuration
import xyz.akhiltay.messenger.resources.*

@Configuration
class JerseyConfig : ResourceConfig() {
    init {
        register(UserResource::class.java)
        register(AuthResource::class.java)
        register(ChatResource::class.java)
        register(MessageResource::class.java)
        register(AttachmentResource::class.java)
        register(MultiPartFeature::class.java)
    }
}
