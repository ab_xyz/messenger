package xyz.akhiltay.messenger

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIncludeProperties
import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.persistence.*
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID

enum class MemberRole {
    OWNER, ADMINISTRATOR, MEMBER
}

@Entity
@Table(name = "users")
class User(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
    @Column(nullable = false, unique = true, length = 32) var username: String,
    @Column(nullable = false, unique = true, length = 320) @JsonIgnore var email: String,
    @Column(length = 256) var name: String = username,
    @Column(length = 512) var bio: String? = null,
    @Column(length = 256) @JsonIgnore var passwordHash: String,
    @UpdateTimestamp @JsonIgnore var updatedAt: Instant? = null,
    @CreationTimestamp @JsonProperty("created_at") var createdAt: Instant? = null
)

@Entity
@Table(name = "chats")
class Chat(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
    @Column(nullable = false, length = 64) var name: String,
    @Column(length = 512) var description: String? = null,
    @UpdateTimestamp @JsonIgnore var updatedAt: Instant? = null,
    @CreationTimestamp @JsonProperty("created_at") var createdAt: Instant? = null
)


@Entity
@Table(name = "chat_members", uniqueConstraints = [UniqueConstraint(columnNames = ["chat_id", "user_id"])])
class ChatMember(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
    @ManyToOne @JoinColumn(name = "chat_id", nullable = false) @JsonIncludeProperties(value = ["id"]) var chat: Chat,
    @ManyToOne @JoinColumn(name = "user_id", nullable = false) @JsonIncludeProperties(value = ["id"]) var user: User,
    @Enumerated(EnumType.STRING) @Column(nullable = false) var role: MemberRole = MemberRole.MEMBER,
    @CreationTimestamp @JsonProperty("created_at") var createdAt: Instant? = null
)

@Entity
@Table(name = "attachments")
class Attachment(
    @Id @GeneratedValue(strategy = GenerationType.UUID) var id: UUID? = null,
    @Column(nullable = false) var filename: String,
    @Column(nullable = false) var contentType: String,
    @Lob @Column(nullable = false) var data: ByteArray,
    @CreationTimestamp @JsonProperty("created_at") var createdAt: Instant? = null,
)

@Entity
@Table(
    name = "messages",
    indexes = [
        Index(name = "idx_messages_chat_id", columnList = "chat_id"),
        Index(name = "idx_messages_chat_member_id", columnList = "chat_member_id")
    ]
)
class Message(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
    @ManyToOne @JoinColumn(name = "chat_id", nullable = false) @JsonIncludeProperties(value = ["id"]) var chat: Chat,
    @ManyToOne @JoinColumn(
        name = "chat_member_id",
        nullable = false
    ) @JsonIncludeProperties(value = ["id"]) var chatMember: ChatMember,
    @Column(nullable = false, columnDefinition = "TEXT") var content: String,
    @Column @JsonProperty("edited_at") var editedAt: Instant? = null,
    @CreationTimestamp @JsonProperty("created_at") var createdAt: Instant? = null,
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "message_attachments", joinColumns = [JoinColumn(name = "message_id")])
    @Column(name = "attachment_id", nullable = false)
    var attachments: List<UUID> = emptyList()
)

@Entity
@Table(name = "tokens")
class Token(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
    @ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "user_id", nullable = false) var user: User,
    @Column(unique = true) var token: String,
    @Column var expiryDate: Instant = Instant.now().plus(14, ChronoUnit.DAYS),
    @CreationTimestamp var createdAt: Instant? = null
)
