package xyz.akhiltay.messenger

import org.mindrot.jbcrypt.BCrypt
import org.springframework.data.domain.PageRequest
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*
import kotlin.jvm.optionals.getOrNull

@Service
class ClientRegistryService {
    private val clients = mutableMapOf<Long, String>()

    fun addClient(userId: Long, token: String) {
        clients[userId] = token
    }

    fun removeClient(userId: Long) {
        clients.remove(userId)
    }

    fun getClients(): Map<Long, String> {
        return clients
    }
}


@Service
class UserService(private val userRepository: UserRepository) {
    fun getAllUsers(): List<User> = userRepository.findAll()
    fun getByEmail(email: String): User? = userRepository.findByEmail(email)
    fun getById(id: Long): User? = userRepository.findById(id).getOrNull()

    @Transactional
    fun createUser(username: String, email: String, name: String, bio: String?, passwordHash: String): User {
        val user = User(username = username, email = email, name = name, bio = bio, passwordHash = passwordHash)
        return userRepository.save(user)
    }
}

@Service
class ChatService(
    private val chatRepository: ChatRepository,
    private val chatMemberRepository: ChatMemberRepository
) {

    fun getAllChats(): List<Chat> = chatRepository.findAll()
    fun getById(id: Long): Chat? = chatRepository.findById(id).getOrNull()

    fun getUserChats(userId: Long) = chatMemberRepository.findChatsByUserId(userId)

    @Transactional
    fun createChat(name: String, description: String?): Chat {
        val chat = Chat(name = name, description = description)
        return chatRepository.save(chat)
    }

    @Transactional
    fun deleteChat(id: Long, deletedByUserId: Long? = null): Boolean {
        if (deletedByUserId != null) {
            val member = chatMemberRepository.findByChatIdAndUserId(id, deletedByUserId)
            if (member == null || member.role != MemberRole.OWNER) return false
            chatRepository.deleteById(id)
        }

        chatMemberRepository.deleteAllByChatId(id)
        chatRepository.deleteById(id)

        return true
    }
}

@Service
class ChatMemberService(
    private val chatMemberRepository: ChatMemberRepository,
    private val chatRepository: ChatRepository,
    private val userRepository: UserRepository
) {
    fun getChatMembers(chatId: Long): List<ChatMember> {
        return chatMemberRepository.findByChatId(chatId)
    }

    @Transactional
    fun addChatMember(chatId: Long, userId: Long, role: MemberRole): ChatMember {
        val chat = chatRepository.findById(chatId).orElseThrow { RuntimeException("Chat not found") }
        val user = userRepository.findById(userId).orElseThrow { RuntimeException("User not found") }
        val chatMember = ChatMember(chat = chat, user = user, role = role)
        return chatMemberRepository.save(chatMember)
    }
}

@Service
class AttachmentService(private val attachmentRepository: AttachmentRepository) {
    fun getAttachmentById(id: UUID): Attachment? = attachmentRepository.findById(id).orElse(null)

    @Transactional
    fun createAttachment(filename: String, contentType: String, data: ByteArray): Attachment {
        val attachment = Attachment(filename = filename, contentType = contentType, data = data)
        return attachmentRepository.save(attachment)
    }
}

@Service
class MessageService(
    private val messageRepository: MessageRepository,
    private val chatRepository: ChatRepository,
    private val chatMemberRepository: ChatMemberRepository,
    private val messagingTemplate: SimpMessagingTemplate,
    private val clientRegistryService: ClientRegistryService
) {
    fun getMessages(chatId: Long, offset: Int = 0, limit: Int = 30): List<Message> {
        val pageable = PageRequest.of(offset / limit, limit)
        val page = messageRepository.findPageByChatIdOrderByCreatedAtDesc(chatId, pageable)
        return page.content
    }

    @Transactional
    fun createMessage(chatId: Long, userId: Long, content: String, attachments: List<UUID> = emptyList()): Message {
        val chat = chatRepository.findById(chatId).orElseThrow { RuntimeException("Chat not found") }
        val chatMember =
            chatMemberRepository.findByChatIdAndUserId(chatId, userId)
                ?: throw RuntimeException("Chat Member not found")
        val message = Message(chat = chat, chatMember = chatMember, content = content, attachments = attachments)

        for (client in clientRegistryService.getClients()) {
            println("WEBSOCKET CLIENT: " + client.key)
        }

        return  messageRepository.save(message).also {
            val clients = clientRegistryService.getClients()
            val chatMembers = chatMemberRepository.findByChatId(chatId)
            chatMembers.forEach { member ->
                val token = clients[member.user.id]
                if (token != null) {
                    messagingTemplate.convertAndSendToUser(token, "/queue/messages", MessageDto.fromEntity(it))
                }
            }
        }
    }
}

@Service
class TokenService(private val tokenRepository: TokenRepository) {
    fun createToken(user: User): Token {
        val token = UUID.randomUUID().toString()
        val expiryDate = Instant.now().plus(14, ChronoUnit.DAYS)
        val tokenEntity = Token(token = token, user = user, expiryDate = expiryDate)
        return tokenRepository.save(tokenEntity)
    }

    fun findByToken(token: String): Token? {
        return tokenRepository.findByToken(token)
    }

    @Transactional
    fun deleteByToken(token: String) {
        tokenRepository.deleteByToken(token)
    }

    fun validateToken(token: String): User {
        val tokenEntity = tokenRepository.findByToken(token)
            ?: throw InvalidTokenException("Token not found or expired")
        if (tokenEntity.expiryDate.isBefore(Instant.now())) {
            throw InvalidTokenException("Token has expired")
        }
        return tokenEntity.user
    }
}

@Service
class AuthService(
    private val userService: UserService,
    private val tokenService: TokenService,
) {

    @Transactional
    fun register(user: UpdateUserDto): Token {
        val u = userService.createUser(
            user.username,
            user.email,
            user.name,
            user.bio,
            BCrypt.hashpw(user.password, BCrypt.gensalt())
        )
        return tokenService.createToken(u)
    }

    fun login(user: LoginUserDto): Token {
        val u = userService.getByEmail(user.email) ?: throw WrongCredentialsException("User not found")
        if (!BCrypt.checkpw(user.password, u.passwordHash)) {
            throw WrongCredentialsException("Password mismatch")
        }

        return tokenService.createToken(u)
    }

    @Transactional
    fun logout(token: String) {
        tokenService.deleteByToken(token)
    }

    fun validateToken(token: String): User {
        return tokenService.validateToken(token)
    }
}