package xyz.akhiltay.messenger

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.Instant
import java.util.*


data class UserDto(
    @JsonProperty("id") val userId: Long?,
    @JsonProperty("username") val username: String,
    @JsonProperty("name") val name: String,
    @JsonProperty("bio") val bio: String?,
    @JsonProperty("created_at") val createdAt: Instant?,
    @JsonProperty("updated_at") val updatedAt: Instant?
) {
    companion object {
        fun fromEntity(user: User) = UserDto(
            userId = user.id,
            username = user.username,
            name = user.name,
            bio = user.bio,
            createdAt = user.createdAt,
            updatedAt = user.updatedAt
        )
    }
}

data class ChatDto(
    @JsonProperty("id") val chatId: Long?,
    @JsonProperty("name") val name: String,
    @JsonProperty("description") val description: String?,
    @JsonProperty("created_at") val createdAt: Instant?,
    @JsonProperty("updated_at") val updatedAt: Instant?
) {
    companion object {
        fun fromEntity(chat: Chat) = ChatDto(
            chatId = chat.id,
            name = chat.name,
            description = chat.description,
            createdAt = chat.createdAt,
            updatedAt = chat.updatedAt
        )
    }
}

data class ChatMemberDto(
    @JsonProperty("id") val chatMemberId: Long?,
    @JsonProperty("chat_id") val chatId: Long?,
    @JsonProperty("user_id") val userId: Long?,
    @JsonProperty("role") val role: MemberRole,
    @JsonProperty("created_at") val createdAt: Instant?
) {
    companion object {
        fun fromEntity(chatMember: ChatMember) = ChatMemberDto(
            chatMemberId = chatMember.id,
            chatId = chatMember.chat.id,
            userId = chatMember.user.id,
            role = chatMember.role,
            createdAt = chatMember.createdAt
        )
    }
}

data class MessageDto(
    @JsonProperty("id") val messageId: Long?,
    @JsonProperty("chat_id") val chatId: Long?,
    @JsonProperty("chat_member_id") val chatMemberId: Long?,
    @JsonProperty("user") val user: UserDto,
    @JsonProperty("content") val content: String,
    @JsonProperty("attachments") val attachments: List<UUID>,
    @JsonProperty("edited_at") val editedAt: Instant?,
    @JsonProperty("created_at") val createdAt: Instant?
) {
    companion object {
        fun fromEntity(message: Message) = MessageDto(
            messageId = message.id,
            chatId = message.chat.id,
            chatMemberId = message.chatMember.id,
            user = UserDto.fromEntity(message.chatMember.user),
            content = message.content,
            attachments = message.attachments,
            editedAt = message.editedAt,
            createdAt = message.createdAt
        )
    }
}

data class TokenDto(
    @JsonProperty("id") val tokenId: Long?,
    @JsonProperty("user_id") val userId: Long?,
    @JsonProperty("token") val token: String,
    @JsonProperty("expiry_date") val expiryDate: Instant,
    @JsonProperty("created_at") val createdAt: Instant?
) {
    companion object {
        fun fromEntity(token: Token) = TokenDto(
            tokenId = token.id,
            userId = token.user.id,
            token = token.token,
            expiryDate = token.expiryDate,
            createdAt = token.createdAt
        )
    }
}

data class UpdateUserDto(
    val email: String,
    val username: String,
    val name: String,
    val bio: String?,
    val password: String
)

data class LoginUserDto(val email: String, val password: String)

data class TokenShortDto(val token: String)

data class CreateChatDto(val name: String, val description: String?)

data class GetChatDto(
    val id: Long,
    val name: String,
    val participantIds: List<Long>
)

data class MessageShortDto(val content: String, val attachments: List<UUID> = emptyList())
