package xyz.akhiltay.messenger.security

import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import xyz.akhiltay.messenger.User

class TokenAuthentication(private val user: User) : Authentication {

    override fun getAuthorities(): Collection<GrantedAuthority> {
        return listOf(SimpleGrantedAuthority("ROLE_USER"))
    }

    override fun setAuthenticated(isAuthenticated: Boolean) {
        // Не реализуется, потому что мы используем простой объект аутентификации
    }

    override fun getName(): String = user.username

    override fun getCredentials(): Any = user.passwordHash

    override fun getPrincipal(): Any = user

    override fun isAuthenticated(): Boolean = true

    override fun getDetails(): Any = user
}
