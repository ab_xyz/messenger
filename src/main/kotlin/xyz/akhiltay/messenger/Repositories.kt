package xyz.akhiltay.messenger

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Repository
interface UserRepository : JpaRepository<User, Long> {
    fun findByUsername(username: String): User?
    fun findByEmail(email: String): User?
}

@Repository
interface ChatRepository : JpaRepository<Chat, Long>

@Repository
interface ChatMemberRepository : JpaRepository<ChatMember, Long> {
    fun findByChatIdAndUserId(chatId: Long, userId: Long): ChatMember?
    fun deleteAllByChatId(chatId: Long)
    fun findByChatId(chatId: Long): List<ChatMember>

    @Query("SELECT cm.chat FROM ChatMember cm WHERE cm.user.id = :userId")
    fun findChatsByUserId(userId: Long): List<Chat>
}

@Repository
interface AttachmentRepository : JpaRepository<Attachment, UUID>

@Repository
interface MessageRepository : JpaRepository<Message, Long> {
    fun findByChatId(chatId: Long): List<Message>
    fun findByChatMemberId(chatMemberId: Long): List<Message>
    fun findPageByChatIdOrderByCreatedAtDesc(chatId: Long, pageable: Pageable): Page<Message>
    fun findByIdAndChatId(id: Long, chatId: Long): Message?
}

@Repository
interface TokenRepository : JpaRepository<Token, Long> {
    fun findByToken(token: String): Token?
    fun deleteByToken(token: String)
}