import {writable} from "svelte/store";
import {fetchChats, fetchMessages, fetchUser} from "$lib/api";
import type {Chat, Message, User} from "$lib/types";
import {browser} from "$app/environment";

export const token = writable<string>(undefined)
export const user = writable<User>(undefined)
export const chats = writable<Chat[]>([])


if (browser) {
    const local = localStorage.getItem("token")
    if (local && local != "undefined") {
        console.log('got token from localstorage')
        token.set(JSON.parse(local) || undefined)
    }

    token.subscribe(value => localStorage.setItem("token", JSON.stringify(value)));


    token.subscribe(async (t) => {
        console.log('Got new token', t)
        user.set(t != null ? await fetchUser(t) : undefined)
        chats.set(t != null ? await fetchChats(t) : undefined)
    })

    user.subscribe(u => {
        console.log("User got update!", u)
    })

    chats.subscribe(c => {
        console.log("Chats updated!", c)
    })

}
