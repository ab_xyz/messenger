export interface User {
    id: number
    username: string
    name: string
    bio?: string
    created_at: Date
    updated_at: Date
}

export interface Chat {
    id: number
    name: string
    members: number
    description: string
}

export enum Role {
    OWNER = "OWNER",
    ADMINISTRATOR = "ADMINISTRATOR",
    MEMBER = "MEMBER",
}

export interface ChatMember {
    id: number
    chat_id: number
    user: User
    role: Role
    created_at: Date
}

export interface Message {
    id: number;
    chat_id: number;
    chat_member_id: number;
    user: User;
    content: string
    attachments: string[]
}
