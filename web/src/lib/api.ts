export const API_URL = "https://04bf30bbdb8d644d7fa160ebd661436d.serveo.net/api";
export const WS_URL = "http://localhost:8080/ws"

// Helper function to handle API requests
async function request(endpoint: string, token: string, options: RequestInit) {
    const response = await fetch(`${API_URL}${endpoint}`, {
        ...options,
        headers: {
            ...options.headers,
            "Authorization": `Bearer ${token}`,
        },
    });
    if (!response.ok) {
        const errorText = await response.text();
        throw new Error(`API request failed with status ${response.status}: ${errorText}`);
    }
    const data = await response.json();
    console.log(`Data from ${endpoint}:`, data);
    return data;
}

// Chat API
export async function fetchChats(token: string) {
    return request("/chats/me", token, {
        method: "GET",
    });
}

export async function fetchMessages(chatId: number, token: string) {
    return request(`/chats/${chatId}/messages`, token, {
        method: "GET",
    });
}

export async function fetchMembers(chatId: number, token: string) {
    return request(`/chats/${chatId}/members`, token, {
        method: "GET",
    });
}

export async function fetchUser(token: string) {
    return request("/auth/me", token, {
        method: "GET",
    });
}

export async function logout(token: string) {
    const response = await fetch(`${API_URL}/auth/logout`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
        },
    });

    if (!response.ok) {
        throw new Error('Logout failed');
    }
}

export async function createMessage(token: string, chatId: number, content: string, attachments: string[] = []) {
    const response = await fetch(`${API_URL}/chats/${chatId}/messages`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify({ content, attachments })
    });

    if (!response.ok) {
        throw new Error('Failed to create message');
    }

    return await response.json();
}

export async function createNewChat(token: string, name: string, description?: string) {
    const response = await fetch(`${API_URL}/chats`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify({ name, description })
    });

    if (!response.ok) {
        throw new Error('Failed to create chat');
    }

    return await response.json();
}

export async function joinChat(token: string, id: number) {
    const response = await fetch(`${API_URL}/chats/${id}/join`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
        },
    });

    if (!response.ok) {
        throw new Error('Chat join failed');
    }

    return await response.json()
}

export async function uploadAttachment(token: string, file: File): Promise<string> {
    const formData = new FormData();
    formData.append('file', file);

    const response = await fetch(`${API_URL}/attachments`, {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${token}`
        },
        body: formData
    });

    if (!response.ok) {
        throw new Error(`Error uploading attachment: ${response.statusText}`);
    }

    const result = await response.json();
    return result.id;
}
